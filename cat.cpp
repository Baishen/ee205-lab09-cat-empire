///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}


void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}
void CatEmpire::dfsInorderReverse(Cat* catFT, int depth) const {

   if( catFT == nullptr ){ 
      return;
   }
   dfsInorderReverse( catFT-> right, depth + 1 );

//print node, Do the work at this (the parent) node

   int nameLen = 6;

   cout << string( nameLen * (depth-1), ' ' ) << catFT->name;

//If Cat node is a leaf node (both left and right == nullptr), just print endl

   if( catFT-> left == nullptr && catFT-> right == nullptr ){
      cout << endl;
   }      

// If node has both left and right print “<“ and endl.

   else if( catFT-> left != nullptr && catFT-> right != nullptr){
      cout << "<"  << endl;
   }

// If node has a left XOR right child, print either “/“ or “\” as appropriate

   else if( catFT-> left != nullptr && catFT-> right == nullptr ){
      cout << " \\ " << endl;
   }
   else if( catFT-> right != nullptr && catFT-> left == nullptr ){
      cout << " / " << endl; 
   }

   dfsInorderReverse( catFT-> left, depth + 1 );
}




void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}

void CatEmpire::dfsInorder(Cat* catL )const{

   if( catL == nullptr ){
      return;
   }
   dfsInorder( catL-> left );

//print node, Do the work at this (the parent) node

   int nameLen = 6;
   cout << string( nameLen, ' ' ) << catL-> name << endl;

dfsInorder( catL-> right );

}




void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}
void CatEmpire::dfsPreorder( Cat* parent )const{
   if( parent == nullptr ){
      return;
   }
//print node
//If the Cat has both a left and right child, print “parent begat left and right” substituting the appropriate cat names.

   if(parent-> left != nullptr && parent-> right != nullptr){
      cout << parent-> name << " begat" << " "  << parent-> left-> name << " and "  << parent-> right-> name << endl;
   }  

//If the Cat has only a left (or right) child, print “parent begat left” or “parent begat right”

   else if(parent-> left != nullptr && parent-> right == nullptr){
      cout << parent-> name << " begat" << " " << parent-> left-> name << endl;
   }

   else if(parent-> left == nullptr && parent-> right != nullptr){
      cout << parent-> name << " begat" << " " << parent-> right-> name << endl;
   }

//If the Cat doesn’t have any children, then don’t print anything.


dfsPreorder( parent->left );

dfsPreorder( parent->right );
}


//void catGenerations() const{
/*   
BFS(root)
 queue<Cat*> catQueue
 catQueue.enqueue(root)
 while catQueue is not empty
 Cat* cat = catQueue.front() // dequeue a cat…
 catQueue.pop()
 if cat is empty then
 return

 if( c->left != nullptr )
 catQueue.push( c->left )
 if( c->right != nullptr )
 catQueue.push( c->right
*/
//}


bool CatEmpire::empty(){
   if (count == 0) {
      return true;
   } else {
      return false;
   }
}

void CatEmpire::addCat( Cat* newCat ){

if(newCat == nullptr){
   return;
   }
if(empty()){ 
   count ++;
   newCat-> left = nullptr;
   newCat-> right = nullptr;
   topCat = newCat;

}

else{
   count++;
   addCat( topCat, newCat );
}
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ){

   if( newCat-> name < atCat-> name){
      if( atCat-> left == nullptr){
      atCat-> left = newCat;
      }
      else{
      addCat( atCat->left, newCat );
      return;
      }
   }
   if( newCat-> name > atCat-> name){
      if( atCat-> right == nullptr)
      {
      atCat-> right = newCat;
      return;
   }
      else{
         addCat( atCat->right, newCat );
            return;
      }
   }

}

