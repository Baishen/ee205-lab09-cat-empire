#include <iostream>
#include "cat.hpp"

int main(){
using namespace std;
   cout << "Hello" << endl;

   CatEmpire cat;

   cout << cat.empty() << endl;
   Cat::initNames();
   Cat* one = Cat::makeCat();
   Cat* dos = Cat::makeCat();
   cat.addCat( one );
   cat.addCat( dos );
   cout << cat.empty() << endl;

const int NUMBER_OF_CATS = 20;

  cout << "Welcome to Cat Empire!" << endl;

	 CatEmpire catEmpire;

	for( int i = 0 ; i < NUMBER_OF_CATS ; i++ ) {
		Cat* newCat = Cat::makeCat();

		catEmpire.addCat( newCat );
	}

	cout << "Print a family tree of " << NUMBER_OF_CATS << " cats" << endl;

	catEmpire.catFamilyTree(); 
}

